all: build docker
build:
	docker build -f Dockerfile.build .
	docker run -v ${HOME}/.glide/:/root/.glide/ -v $$(pwd)/release:/mnt/release --rm $$(docker build  -qf Dockerfile.build .)
docker:
	docker build -t vxlabs/iot-homes  .
localbuild:
	go get ./... && go test ./... && CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./release/server ./cmd/userapi
deploy:
	docker run --rm \
	    -e DOCKER_REGISTRY=$$DOCKER_REGISTRY \
        -e KUBE_URL=$$KUBE_URL \
        -e KUBE_NAMESPACE=$$IOT_KUBE_NAMESPACE \
        -e KUBE_TOKEN=$$IOT_KUBE_TOKEN \
        -e COMMIT_HASH=$$CI_COMMIT_SHA \
        -e ENVIRONMENT_PUBLIC_NAME=homes.$$IOT_ENVIRONMENT_NAME \
        -e APPROLE_ID=$$IOT_HOMES_APPROLE_ID \
        -e APPROLE_SECRET=$$IOT_HOMES_APPROLE_SECRET \
        -e KAFKA_BROKERS=$$KAFKA_BROKERS \
        -v $$(pwd)/kubernetes-spec.yml.template:/media/template:ro \
        ${DOCKER_REGISTRY}/vxlabs/k8s-deploy
