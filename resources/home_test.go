package resources

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func Test_Home(t *testing.T) {
	h := Home{}
	assert.NotNil(t, h)
}

func Test_Home_Validate(t *testing.T) {
	h := &Home{}
	assert.False(t, h.Validate())
	h = &Home{
		Name: "test",
	}
	assert.True(t, h.Validate())
}
