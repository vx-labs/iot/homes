package resources

import (
	"time"
	"github.com/vx-labs/identity-types/server"
	"encoding/json"
)

type Home struct {
	Id      string    `json:"id" indexed:"uuuid,unique"`
	Owner   string    `json:"owner" indexed:"string"`
	Created time.Time `json:"created"`
	Updated time.Time `json:"updated"`
	Name    string    `json:"name" indexed:"string"`
}

func (h *Home) UniqueId() string {
	return h.Id
}

func (h *Home) OwnerId() string {
	return h.Owner
}

func (h *Home) Validate() bool {
	return h.Name != ""
}
func (h *Home) WithOwner(owner string) server.Resource {
	c := *h
	c.Owner = owner
	return &c
}

func (h *Home) WithId(id string) server.Resource {
	c := *h
	c.Id = id
	return &c
}

func (h *Home) WithUpdated(t time.Time) server.Resource {
	c := *h
	c.Updated = t
	return &c
}
func (h *Home) WithCreated(t time.Time) server.Resource {
	c := *h
	c.Created = t
	return &c
}

func (h *Home) FromJSON(b []byte) error {
	return json.Unmarshal(b, h)
}
func (h *Home) ToJSON() ([]byte, error) {
	return json.Marshal(h)
}
