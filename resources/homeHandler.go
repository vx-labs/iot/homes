package resources

import (
	"github.com/vx-labs/identity-types/server"
	"github.com/vx-labs/go-rest-api"
	"github.com/vx-labs/identity-api/store"
)

type homeHandler struct {
	state    server.StateStore
	events   chan server.Event
	commands chan server.Event
}

func HomeHandler() server.ResourceHandler {
	h := &homeHandler{
		events: make(chan server.Event),
		commands: make(chan server.Event),
	}
	h.state = store.NewMemoryStoreFromResource(h.Schema())
	return h
}

func (h *homeHandler) Store() server.StateStore {
	return h.state
}
func (h *homeHandler) Schema() server.Resource {
	return &Home{}
}

func (h *homeHandler) Events() chan server.Event {
	return h.events
}
func (h *homeHandler) Commands() chan server.Event {
	return h.commands
}
func (h *homeHandler) EventsSubscriptions() map[string]server.EventHandler {
	return map[string]server.EventHandler{}
}
func (h *homeHandler) CommandsSubscriptions() map[string]server.EventHandler {
	return map[string]server.EventHandler{}
}
func (h *homeHandler) Grammar() api.Grammar {
	return api.Grammar{
		Plural:   "homes",
		Singular: "home",
	}
}
