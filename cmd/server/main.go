package main

import (
	"context"
	api "github.com/vx-labs/go-rest-api/client"
	"github.com/vx-labs/identity-api/authentication"
	"github.com/vx-labs/iot-homes/resources"
	"github.com/vx-labs/go-rest-api/logger"
)

func main() {
	l := api.NewLogger()
	ctx := context.Background()
	ctx = logger.Store(ctx, l.WithField("source", "root_context"))

	auth, err := authentication.NewClient(ctx, l.WithField("source", "authentication_client"))
	if err != nil {
		l.Fatalf(err.Error())
	}

	server, err := auth.NewResourceServer(ctx, l.WithField("source", "service"), "iot", "v1")
	if err != nil {
		l.Fatalf(err.Error())
	}
	server.AddResource("/", resources.HomeHandler())
	l.Error(server.ListenAndServe(ctx, 8006))
}
